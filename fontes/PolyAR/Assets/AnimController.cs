﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    bool folded = true;

    Animator anim;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
    public void Animate()
    {
        if (folded)
        {
            anim.Play("Opening", -1, 0f);
            folded = !folded;
        }
        else
        {
            anim.Play("Closing", -1, 0f);
            folded = !folded;
        }
    }
}
