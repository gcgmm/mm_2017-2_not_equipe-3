﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetModel : MonoBehaviour
{
    private GameObject imageTarget;
    private Transform trans;

    private void Start()
    {
        imageTarget = GameObject.FindGameObjectWithTag("ImageTarget");
        trans = imageTarget.transform.GetChild(0);
    }

    public void ResetMod()
    {
        trans.localScale = new Vector3(1, 1, 1);
    }
}
