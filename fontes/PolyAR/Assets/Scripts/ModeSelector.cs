﻿using UnityEngine;
using UnityEngine.UI;

public class ModeSelector : MonoBehaviour
{
    public int currentMode = 0;

    public Button foldButton;
    
	// Use this for initialization
	void Start ()
    {
        SelectMode();
	}
	
	// Update is called once per frame
	void Update ()
    {
        int previousMode = currentMode;

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (currentMode >= transform.childCount - 1)
                currentMode = 0;

            currentMode++;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (currentMode <= 0)
                currentMode = transform.childCount - 1;

            currentMode++;
        }

        if (previousMode != currentMode)
        {
            SelectMode();
        }
    }

    void SelectMode()
    {
        int i = 0;

        foreach (Transform model in transform)
        {
            if (i == currentMode)
            {
                model.gameObject.SetActive(true);

                if (model.transform.GetComponentInChildren<AnimController>() != null)
                {
                    foldButton.gameObject.SetActive(true);
                }
                else
                {
                    foldButton.gameObject.SetActive(false);
                }
            }
            else
            {
                model.gameObject.SetActive(false);
            }
            i++;
        }
    }

    public void UpdateMode()
    {
        if (currentMode >= transform.childCount - 1)
            currentMode = 0;
        else
            currentMode++;

        SelectMode();
    }
}
