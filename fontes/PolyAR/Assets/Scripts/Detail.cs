﻿using UnityEngine;

public class Detail : MonoBehaviour
{
    public enum Type
    {
        Vertex,
        Edge,
        Face
    }

    public bool isEnabled = false;
    public Type type;
}
