﻿using UnityEngine;

public class DetailSelector : MonoBehaviour
{
    public Transform otherMode;

    int detailMask;
    float camRayLenght = 100f;

    bool isActive = false;

    private void Awake()
    {
        detailMask = LayerMask.GetMask("DetailSelect");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SelectDetail();
        }
    }

    private void SelectDetail()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit detailHit;

        if (Physics.Raycast(camRay, out detailHit, camRayLenght, detailMask))
        {
            Transform detail = detailHit.collider.GetComponent<Transform>();
            Detail state = detailHit.collider.GetComponent<Detail>();

            if (detail != null)
            {
                MeshRenderer mat = detail.GetComponent<MeshRenderer>();

                if (state.isEnabled)
                {
                    if (state.type == Detail.Type.Face)
                    {
                        mat.material.color = new Color(255, 255, 0, 255);
                        state.isEnabled = state.isEnabled ? false : true;
                    }
                    else
                    {
                        mat.material.color = new Color(mat.material.color.r, mat.material.color.g, mat.material.color.b, 255);
                        state.isEnabled = state.isEnabled ? false : true;
                    }

                    //foreach (Transform child in otherMode)
                    //{
                    //    if (child.name == detail.name)
                    //    {
                    //        MeshRenderer childMat = child.GetComponent<MeshRenderer>();
                    //        childMat.material.color = new Color(childMat.material.color.r, childMat.material.color.g, childMat.material.color.b, 0);
                    //        isActive = isActive ? false : true;
                    //    }
                    //}
                }
                else
                {
                    if (state.type == Detail.Type.Face)
                    {
                        mat.material.color = new Color(100, 100, 100, 255);
                        state.isEnabled = state.isEnabled ? false : true;
                    }
                    else
                    {
                        mat.material.color = new Color(mat.material.color.r, mat.material.color.g, mat.material.color.b, 0);
                        state.isEnabled = state.isEnabled ? false : true;
                    }
                    //mat.material.color = new Color(mat.material.color.r, mat.material.color.g, mat.material.color.b, 255);
                    //state.isEnabled = state.isEnabled ? false : true;
                }
            }
        }
    }
}
