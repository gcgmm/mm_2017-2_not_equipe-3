using UnityEngine;
	using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public int timeLoadNextLevel;

    void Start()
    {
        if (timeLoadNextLevel == 0)
        {
            Debug.Log("Auto level load disabled");
        }
        else
        {
            Invoke("LoadNextLevel", timeLoadNextLevel);
            timeLoadNextLevel = 0;
        }
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void LoadNextLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

        if (SceneManager.sceneCount >= nextSceneIndex)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    public void LoadPreviousLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex - 1;

        if (nextSceneIndex >= 0)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.sceneCount - 1);
        }
    }

    public void QuitRequest()
    {
        Application.Quit();
    }
}
